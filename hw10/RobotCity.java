
import java.util.Arrays;
import java.util.Random;

public class RobotCity{
   public static void main(String args[]){
        for(int i=0;i<5;i++){
        Random random = new Random();
        //creating the city 
        int[][] city=buildCity();
        //the city
        System.out.println("before the invasion:");
        display(city);
        //number of probots 
        int k=random.nextInt(city.length * city[0].length);
        // after the invasion of the robots
        int[][] After=invade(city,k);
        // the new city
        System.out.println("");
        System.out.println("after the invasion:");
        display(After);
        //city after the shift
        int[][] cityAfter=update(After);
        System.out.println("");
        System.out.println("after the shift:");
       display(cityAfter);
        }
    }
    public static int[][] buildCity(){
        
        Random random = new Random();
        //the width of the city 
        int width = random.nextInt(6)+10;
        //the height of the city
        int height = random.nextInt(6)+10;
       
        int[][] city= new int[height][width];
        //creates the city array 
        for(int i=0;i<height;i++){
            for(int k=0;k<width;k++){
                city[i][k]=random.nextInt(800)+100;
            }
        }
        
        return city;
    }
    
    public static void display(int[][] x){
        for(int i = 0; i < x.length; i++){
           for(int k = 0; k < x[0].length; k++){
              System.out.printf(" %s ", x[i][k]);
            }
            System.out.println(" ");
        }
    }
    public static int[][] invade(int[][] city,int k){
        
        Random random = new Random();
        for(int i=0;i<k;i++){
            //where robot lands
            int width=random.nextInt(city[0].length);
            int height=random.nextInt(city.length);
            //makes robot not land in the same place
            if(city[height][width]<0){
                i--;
            }
            else{
                city[height][width]= - city[height][width];
            }
        }
        //returning array
        return city;

    }
    public static int[][] update(int[][] city){
        for(int i=0;i<city.length;i++){
            for(int k=0;k<city[0].length;k++){
                if(city[i][k]<1 && k>0){
                    city[i][k]=-city[i][k];
                    city[i][k-1]=-city[i][k-1];
                }
            }
        }
        return city;
    }
   
}