//Quang Tran
//CSE02
//May 2 
import java.util.Arrays;
import java.util.*;
import java.util.Scanner;
 
public class Straight{
  public static void main (String args[]){
    int [] deck1;
    deck1 = randomDeck(); //calls the method randomDeck
    int [] hand1;
    hand1 = drawHand(deck1); //calls the method drawHand
    
   
    
    boolean isStraight= true;
    for(int i = 1; i<hand1.length; i++){ // checks if there is a straight
      
      if((search(hand1,i+1)-search(hand1, i))>1){
         isStraight=false;
      }
      
    }
 if (isStraight==true){
   System.out.println("This is a straight");
 }
    else{
       System.out.println("This is not a straight");
    }
  }

 
    public static int[] randomDeck(){
       
        Random random= new Random();
        int [] deck = new int[52];   
        int x=0;
        for(int i=0;i<deck.length;i++){
            //inputs number into deck
            deck[i]=x;

            
            x++;
        }
    // loop to shuffle the deck
        for (int i=0; i<deck.length; i++) {

           
            int position = random.nextInt(deck.length);

            //making sure the array will not get the same numbers
            int trash = deck[i];
            // shuffling 
            deck[i] = deck[position];
            deck[position] = trash;
            
        }
       return deck;
    }
       
public static int[] drawHand(int[] x){
        
      int [] hand=new int[5];

        //for loop that makes sure the hand is drawn properly
        for(int i=0;i<5;i++){
          hand[i]=x[i];
        }
  return hand;
}
public static int search (int [] hand, int k){
  for(int i=0; i<hand.length-1; i++){
    int index=i;
    for(int j=i+1; j<hand.length;j++){ //swapping values to arrange the hand array from smallest to biggest
      if(hand [j] < hand [index]){
        index=j;
        int temp = hand [index];
        hand [index]=hand [i];
        hand[i]=temp;
      }
    }
  }
  return hand[k-1]; //returns the kth position lowest value
}
  
    }
 