// Quang Tran
// 02/08/2019
// CSE02-section 19
// this programs finds the original cost of the check, the percentage tip they wish to pay, and the number of ways the check will be split and determines how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner;
// import scanner class

public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
       
        Scanner myScanner = new Scanner( System.in );// tells Scanner that you are creating an instance that will take input from STDIN
          //inputs 
         	System.out.print("enter The Percentage Tip That You Wish To Pay As A Whole Number (in The Form xx.xx):" );


        double tipPercent = myScanner.nextDouble();
tipPercent /= 100; //We want to convert the percentage into a decimal value
  System.out.print("enter The Number Of People Who Went Out To Dinner");
int numPeople = myScanner.nextInt();
          double totalCost;
          
          System.out.print("enter the cost of check");
double checkCost = myScanner.nextInt();
          
          
          
 //Output
double costPerPerson;
int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); 
               





          

}  //end of main method   
  	} //end of class
