//Quang Tran
//April 16th
//HW09

import java.util.Arrays;
import java.util.*;
import java.util.Scanner;

public class ArrayGames{
   public static void main(String[] args) {
        //initializing the scanner
        Scanner myScanner=new Scanner(System.in);
        //asks the user what path they want 
        System.out.println("to combine the arrays write combine, to shorten the array write shorten");
        String x=myScanner.next();
        if(x.equals("combine")){ //generates array for combine path
            int[] arry=generate();
            int[] arry1=generate();
            int[] cArray=insert(arry,arry1);
            print(arry);
            print(arry1);
            print(cArray);
            
        }
        else if(x.equals("shorten")){ //creates array for shorren path
            
            int[] arry2=generate();
            System.out.println("enter where you want to shorten it");
            int location=myScanner.nextInt(); 
            if(arry2.length<location){
            print(arry2);
            }
            else{
            int[] shortArray=shorten(arry2,location);
            print(arry2);
            print(shortArray);
            }
        }
    
       
    }
    public static int[] generate(){
        Random random = new Random();

        //generate randome length for array
        int random1=(int)(random.nextInt(21));
    
        //keeps length between 10 and 20
        while(random1<10){
            random1=(int)(random.nextInt(21));
        }
        
          int[] arry4= new int[random1];

        //creates the random numbers for the array
        for(int i=0; i < random1; i++){
            int buffer=(int)(random.nextInt(21));
            arry4[i]=buffer;
        }
        
        return arry4;
        
    }
    public static void print(int[] x){ 
        System.out.print("input: ( ");

        for(int i=0;i<x.length;i++){
            System.out.print(x[i]);
            System.out.print(", ");
        }
        System.out.println(")");
        
    }
   
        
    public static int[] insert(int[] x, int[] y){
        Random random = new Random();
        //which character of the array is being replaced randomly
        int replaced=(int)(random.nextInt(x.length));

        //the combined array
        int[] array= new int[x.length+y.length];

       
        int[] leftover= new int[x.length-replaced];
        
        for(int i=0; i<x.length; i++){
            if(i<replaced){
                array[i]=x[i];
            }
            else{
                leftover[i-replaced]=x[i];
            }
        }
        int j=0;
        for(int b=x.length-leftover.length;b<x.length+y.length-leftover.length; b++){
            array[b]=y[j];
            j++;
        }
        int s=0;
        for(int a=x.length+y.length-leftover.length;a<x.length+y.length;a++){
            array[a]=leftover[s];
            s++;
        }
        return array;


    }
    public static int[] shorten(int[] h, int location){
        int[] array= new int[location];
        //cuts the array  
        for(int i=0;i<location;i++){
        array[i]=h[i];
        }
        return array;
    }
}