//Quang Tran
//HW06
//March 19 2019

import java.util.Scanner;//import scanner
public class Network{//class
 public static void main(String[] args) {//main method
    
    Scanner scan = new Scanner(System.in);
   
    System.out.println("input course width");//ask for width
    int width;//initialize width
    while(!scan.hasNextInt()){ // loop to check if user input a valid entry 
      System.out.println("Enter a valid width");//tells user to put in a valid input if wrong 
      scan.next();
    }
    width=scan.nextInt();
   
  
    System.out.println("input length");
    while(!scan.hasNextInt()){ // loop to check if user input a valid entry 
      System.out.println("Enter a valid length");//tells user to put in a valid input if wrong 
      scan.next();
    }
    int length=scan.nextInt();
   
   System.out.println("input size");
    int size;
    while(!scan.hasNextInt()){// loop to check if user input a valid entry 
      System.out.println("Enter a valid int");//tells user to put in a valid input if wrong 
      scan.next();
    }
    size=scan.nextInt();
   
   System.out.println("input edge length");
   int edge; //scan.nextInt();
     while(!scan.hasNextInt()){// loop to check if user input a valid entry 
      System.out.println("Enter a valid int");//tells user to put in a valid input if wrong 
      scan.next();
    }
    edge=scan.nextInt();
   
  
     int gridSize = size + edge;
        boolean isEven = (size % 2 == 0);
        
        int edgeUpper = 0;
        int edgeLower = 0;
        
        // even case for edge positions
        if(isEven) {
            edgeUpper = size / 2 - 1;
            edgeLower = size / 2;
        }
        
        // odd case
        else {
            edgeUpper = size / 2;
        }
    
        for(int i = 0; i < length; i++) {
            String buffer = "";
            
            for(int k = 0; k < width; k++) {
                
                // top or bottom of box
                if(i % gridSize == 0 || i % gridSize == size - 1) {
                    
                    // actually in box
                    if(k % gridSize >= 0 && k % gridSize <= size - 1) {
                        
                        // corner
                        if(k % gridSize == 0 || k % gridSize == size - 1) {
                            buffer += "#";
                        }
                        
                        // edge
                        else {
                            buffer += "-";
                        }
                    }
                    else {
                        buffer += " ";
                    }
                }
                else {
                    
                    // vertical edge of box
                    // vertical bound check
                    if(i % gridSize >= 0 && i % gridSize <= size - 1) {
                        
                        // horiz bound check
                        if(k % gridSize == 0 || k % gridSize == size - 1) {
                            buffer += "|";
                        }
                        else {
                            
                            // horizontal edges, same check as below
                            if((i % gridSize == edgeUpper || (isEven && i % gridSize == edgeLower)) &&
                                    !(k % gridSize >= 0 && k % gridSize <= size - 1)) {
                                buffer += "-";
                            }
                            else {
                                buffer += " ";
                            }
                        }
                    }
                    else {
                        
                        // check for vertical edges (handles even and odd)
                        if(k % gridSize == edgeUpper || (isEven && k % gridSize == edgeLower)) {
                            buffer += "|";
                        }
                        else {
                            buffer += " ";
                        }
                    }
                }
            }
            
            System.out.println(buffer);
        }
       
     }
   }
   
   
   
   