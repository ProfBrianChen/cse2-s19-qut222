// Quang Tran
// 02/17/2019
// CSE02-section 19
//this programs picks a random card out of a 52 card deck
public class CardGenerator{
  public static void main(String[] args) {
    int Num=(int)(Math.random()*52+1);// gets a random number between 1-52
    String cardValue="";
    //using the switch command to get the face card values
      switch(Num%13){
        case 1:
          cardValue="Ace";
        break;
        case 11:
            cardValue="Jacks";
          break;
        case 12:
          cardValue="Queens";
          break;
        case 0:
          cardValue="Kings";
           break; 
        default:
          cardValue=String.valueOf(Num%13); // using the default to get the other values when it isn't a face card
          break;
           }
    // using else if to determine suits
    String Suits="";
      if (Num>=1&&Num<14){Suits="Clubs";
                         }
    else if (Num>=14&&Num<27){Suits="Spades";
                             }
    else if (Num>=27&&Num<40){Suits="Hearts";
                             }
    else if (Num>=40&&Num<53){Suits="Diamonds";
                             }
    System.out.println("The card chosen is "+cardValue+" of "+Suits); //prints out the suit and value of the random card
    
   
        
  }
}