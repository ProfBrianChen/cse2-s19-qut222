//Quang Tran
//02/18/19
//CSE2 section 19
//This program identfifies a poker hand that is randomly generated 

import java.util.Scanner; 
public class PokerHandCheck {
  public static void main(String[] args){
    //Generating 5 random card from 5 different decks
      int Num1=(int)(Math.random()*52+1);// gets a random number between 1-52
    String card1Value="";
    //using the switch command to get the face card values
      switch(Num1%13){
        case 1:
          card1Value="Ace";
        break;
        case 11:
            card1Value="Jacks";
          break;
        case 12:
          card1Value="Queens";
          break;
        case 0:
          card1Value="Kings";
           break; 
        default:
          card1Value=String.valueOf(Num1%13); // using the default to get the other values when it isn't a face card
          break;
           }
    // using else if to determine suits
    String Suits1="";
      if (Num1>=1&&Num1<14){Suits1="Clubs";
                         }
    else if (Num1>=14&&Num1<27){Suits1="Spades";
                             }
    else if (Num1>=27&&Num1<40){Suits1="Hearts";
                             }
    else if (Num1>=40&&Num1<53){Suits1="Diamonds";
                             }
    System.out.println("The card chosen is "+card1Value+" of "+Suits1); //prints out the suit and value of the random card
    
   int Num2=(int)(Math.random()*52+1);// gets a random number between 1-52
    String card2Value="";
    //using the switch command to get the face card values
      switch(Num2%13){
        case 1:
          card2Value="Ace";
        break;
        case 11:
            card2Value="Jacks";
          break;
        case 12:
          card2Value="Queens";
          break;
        case 0:
          card2Value="Kings";
           break; 
        default:
          card2Value=String.valueOf(Num2%13); // using the default to get the other values when it isn't a face card
          break;
           }
    // using else if to determine suits
    String Suits2="";
      if (Num2>=1&&Num2<14){Suits2="Clubs";
                         }
    else if (Num2>=14&&Num2<27){Suits2="Spades";
                             }
    else if (Num2>=27&&Num2<40){Suits2="Hearts";
                             }
    else if (Num2>=40&&Num2<53){Suits2="Diamonds";
                             }
    System.out.println("The card chosen is "+card2Value+" of "+Suits2); //prints out the suit and value of the random card
    
   int Num3=(int)(Math.random()*52+1);// gets a random number between 1-52
    String card3Value="";
    //using the switch command to get the face card values
      switch(Num3%13){
        case 1:
          card3Value="Ace";
        break;
        case 11:
            card3Value="Jacks";
          break;
        case 12:
          card3Value="Queens";
          break;
        case 0:
          card3Value="Kings";
           break; 
        default:
          card3Value=String.valueOf(Num3%13); // using the default to get the other values when it isn't a face card
          break;
           }
    // using else if to determine suits
    String Suits3="";
      if (Num3>=1&&Num3<14){Suits3="Clubs";
                         }
    else if (Num3>=14&&Num3<27){Suits3="Spades";
                             }
    else if (Num3>=27&&Num3<40){Suits3="Hearts";
                             }
    else if (Num3>=40&&Num3<53){Suits3="Diamonds";
                             }
    System.out.println("The card chosen is "+card3Value+" of "+Suits3); //prints out the suit and value of the random card
    
     int Num4=(int)(Math.random()*52+1);// gets a random number between 1-52
    String card4Value="";
    //using the switch command to get the face card values
      switch(Num4%13){
        case 1:
          card4Value="Ace";
        break;
        case 11:
            card4Value="Jacks";
          break;
        case 12:
          card4Value="Queens";
          break;
        case 0:
          card4Value="Kings";
           break; 
        default:
          card4Value=String.valueOf(Num4%13); // using the default to get the other values when it isn't a face card
          break;
           }
    // using else if to determine suits
    String Suits4="";
      if (Num4>=1&&Num4<14){Suits4="Clubs";
                         }
    else if (Num4>=14&&Num4<27){Suits4="Spades";
                             }
    else if (Num4>=27&&Num4<40){Suits4="Hearts";
                             }
    else if (Num4>=40&&Num4<53){Suits4="Diamonds";
                             }
    System.out.println("The card chosen is "+card4Value+" of "+Suits4); //prints out the suit and value of the random card
    
   int Num5=(int)(Math.random()*52+1);// gets a random number between 1-52
    String card5Value="";
    //using the switch command to get the face card values
      switch(Num5%13){
        case 1:
          card5Value="Ace";
        break;
        case 11:
            card5Value="Jacks";
          break;
        case 12:
          card5Value="Queens";
          break;
        case 0:
          card5Value="Kings";
           break; 
        default:
          card5Value=String.valueOf(Num5%13); // using the default to get the other values when it isn't a face card
          break;
           }
    // using else if to determine suits
    String Suits5="";
      if (Num5>=1&&Num5<14){Suits5="Clubs";
                         }
    else if (Num5>=14&&Num5<27){Suits5="Spades";
                             }
    else if (Num5>=27&&Num5<40){Suits5="Hearts";
                             }
    else if (Num5>=40&&Num5<53){Suits5="Diamonds";
                             }
    System.out.println("The card chosen is "+card5Value+" of "+Suits5); //prints out the suit and value of the random card

 //checking the cards to see what hand we have
    int counter = 0; //counts the # of pair
    
//keep adding counter by 1 everytime we encouter a pair
if (card1Value.equals(card2Value)){
  counter = counter+1;
}
    if(card1Value.equals(card3Value)){
      counter = counter + 1;
    }
     if(card1Value.equals(card4Value)){
       counter = counter + 1;
     }
       if(card1Value == (card5Value)){
          counter = counter + 1;
       }
 
   
 if(card2Value.equals(card3Value)){
    counter = counter + 1;
 }
 if(card2Value.equals(card4Value)){
      counter = counter + 1;
 }
  if(card2Value.equals(card5Value)){
       counter = counter + 1;
  }
        

 
  if(card3Value.equals(card4Value)){
    counter = counter + 1;
  }
   if(card3Value.equals(card5Value)){
      counter = counter + 1; 
      }
    
  if(card4Value.equals(card5Value)){
    counter = counter + 1;}
    
   //prints out the hand we have.  
    if(counter==0){ System.out.println ("You have a high card");
                  }
  if(counter==1){ System.out.println ("You have a pair ");
                }
   if(counter==2){ System.out.println ("You have a 2 pair ");
  }
    if(counter==3){ System.out.println ("You have a 3 of a kind ");
  }
                
  
  
                

   
    
   
  }
}
                 