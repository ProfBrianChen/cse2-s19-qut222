//Quang Tran
//CSE 02
//03/26.19


import java.util.Scanner;//import scanner 
public class Area{
  //method for area of triangle
  public static double triangle (double height, double base){
    double area= (0.5)*(height*base);
       return(area);
  }
  //method for area of rectangle
   public static double rectangle (double length, double width){
    double area= width*length;
    return(area);
  }
  //method for area of circle
   public static double circle (double r){
    double area= 3.14*(r*r) ;
    return(area);
  }
  //method for input 
   public static void input(String shape){
    Scanner scan= new Scanner(System.in);
    boolean i = true;
   
     //loop to ask user to reinput if wrong
    while(i){ 
          if(shape.equals("triangle")){ //triangle scenario
           i=false;
            System.out.println("enter dimensions for width");
           while(!scan.hasNextDouble()){
            System.out.println("enter a valid width");
           scan.next();
           }
           double width=scan.nextDouble();
            System.out.println("enter dimensions for height");
          while(!scan.hasNextDouble()){
            System.out.println("enter a valid height");
            scan.next();
          }
            double height=scan.nextDouble();
           double area = triangle (width, height);//calls the triangle method
            System.out.println("the area of the triangle is"+" "+area);//prints final output
           }
          
      else if(shape.equals("rectangle")){ //rectangle scenario
             i=false;
            System.out.println("enter dimensions for width");
           while(!scan.hasNextDouble()){
            System.out.println("enter a valid width");
           scan.next();
           }
           double width=scan.nextDouble();
            System.out.println("enter dimensions for length");
          while(!scan.hasNextDouble()){
            System.out.println("enter a valid length");
            scan.next();
          }
            double length=scan.nextDouble();
           double area = rectangle (width, length);//calls the rectangle method
            System.out.println("the area of the triangle is"+" "+area);
           }
          
     else if(shape.equals("circle")){ //circle scenario
             i=false;
            System.out.println("enter radius of the circle");
       while(!scan.hasNextDouble()){
            System.out.println("enter a valid radius");
           scan.next();
           }
           double r=scan.nextDouble();
           double area = circle(r);//calls the circle method
            System.out.println("the area of the triangle is"+" "+area);
       
          }
     else{ //invalid shape scenario
           System.out.println("enter a valid shape: Triangle, Rectangle, Square");
      shape=scan.next();
       

        }
    }
   }
  //main method
  public static void main (String[] args){
     Scanner scan= new Scanner(System.in);
    System.out.println("enter a shape"); 
    String shape = scan.next();// ask for the shape
    
    input(shape); //calls the input method

   
  }
}