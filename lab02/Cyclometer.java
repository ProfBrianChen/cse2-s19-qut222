// Quang Tran
// 02/01/2019
// CSE02-section 19
// This program measures is a bicycle cyclometer and will measure time elapsed in seconds and will measure the number of rotations in the front wheel during that time 

public class Cyclometer {
  public static void main(String[] args)  {
   //input data
    int secsTrip1=480;  //Counts time in second for first trip
    int secsTrip2=3220;  //counts time in second for second trip
		int countsTrip1=1561;  //counts rotations of front wheel on first trip 
		int countsTrip2=9037; //counts rotation of front wheel on second trip
    // our intermediate variables and output data
     double wheelDiameter=27.0,  
  	PI=3.14159, 
  	feetPerMile=5280,  
  	inchesPerFoot=12,  
  	secondsPerMinute=60; // usefull constants
	  double distanceTrip1, distanceTrip2,totalDistance;  //future variables for the calculation

System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
//run the calculations; store the values. Document your

	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
	
//Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
 
    
  } 
}