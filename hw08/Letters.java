//Quang Tran
//CSE02
//4/9/19

import java.util.Random;
import java.util.Arrays;
public class Letters{
	public static void main(String args[]){
  char [] AtoM;
  char [] NtoZ;
	
	
	//fill array 
	Random rand = new Random();
	int CharArrayLength=(int)(rand.nextInt(60));
	char[] randomCharArry = new char[CharArrayLength];//declare array
	for(int i=0; i<CharArrayLength; i++) {
		char character= (char)(rand.nextInt(26)+'a');//generates character
		boolean x = rand.nextBoolean();
		if (x == true) { //randomly generates capital letters in sequence
			character= Character.toUpperCase(character);
			
			
   
	}
    randomCharArry[i]=character;
	}
  System.out.println(randomCharArry);//prints out original array
	AtoM=GetAtoM(randomCharArry,CharArrayLength);
	System.out.println(AtoM);// prints out Array with A to M
	NtoZ=GetNtoZ(randomCharArry,CharArrayLength);
	System.out.println(NtoZ);// prijtws out Array with N to Z
	
  }
	public static char[]  GetAtoM(char [] randomCharArry,int CharArrayLength){ // method for A to M
		char[] AtoM =new char[CharArrayLength];
	for(int i=0; i<CharArrayLength;i++) {
    if ((randomCharArry[i]>='a' && randomCharArry[i]<='m') || randomCharArry[i]>='A' && randomCharArry[i]<='M'){ // checks if the letters are less than M
    	AtoM[i]= randomCharArry[i];
    }
   }
   return AtoM;
}
	public static char[]  GetNtoZ(char[] randomCharArry,int CharArrayLength){//method for N to Z
		char[] NtoZ =new char[CharArrayLength];
	for(int i=0; i<CharArrayLength;i++) {
    if ((randomCharArry[i]>='n' && randomCharArry[i]<='z') || randomCharArry[i]>='N' && randomCharArry[i]<='Z'){ // checks if letters are more than M
    	NtoZ[i]= randomCharArry[i];
    }
   }
   return NtoZ;
}
	}